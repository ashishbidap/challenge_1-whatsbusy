# -*- coding: utf-8 -*-
"""graph_directed.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1XmFes2q-Rf6uW9sB0PeR81LapTAk4egE

Problem Overview: Network Failure Point <br>
We have a mesh network connected by routers labeled from 1 to 6 in a directed manner. Write an algorithm that can detect the routers with the highest number of connections so we might know which routers will cause a network failure if removed. Multiple connections between the same routers should be treated as separate connections. A tie for the most number of connections is possible. 

Requirements<br>
Implement a identify_router function that accepts an input graph of nodes representing the total network and identifies the node with the most number of connections. 
Return the label of the node. 
Implement a directed graph data structure using Python 3.6 and up.
Each node is unique thus there will be no cases of having multiple nodes with the same label.
Each node will have an infinite number of both inbound and outbound links.
"""

class Graph:
  def __init__(self,edges):
    self.Nodes = []
    self.adj_list = {}
    self.adj_list_count={}
    self.edges=edges
    self.is_directed=True

    #creating a unique set of Nodes
    for i in range(len(self.edges)):
      self.Nodes.append(self.edges[i][0])
      self.Nodes.append(self.edges[i][1])
    self.Nodes = set(self.Nodes)

    for node in self.Nodes:
      self.adj_list[node]=[]

  def add_edge(self,x,y,is_directed=True):
    """
      1.Method to add edges to the adjacency list.
      2.is_directed=true for directed graph
    """
    self.adj_list[x].append(y)
    if not is_directed:   
      self.adj_list[y].append(x)

  def graph_display(self):
    print(self.adj_list_count)

  def identify_router(self):
    max=0       
    #adding edges to the adjecency list
    for i,j in self.edges:
      self.add_edge(i,j) 

    #counting the number of outgoing links for each vertex
    for key,val in self.adj_list.items():
      self.adj_list_count[key] = len(val)  
    
    #checking for incoming links and incrementing the count of adj_list_count
    for i in self.adj_list:
      for j in self.adj_list:
        if i in self.adj_list[j]:
          self.adj_list_count[i]=self.adj_list_count[i] + 1
    
    #Finding the key with maximum number of links
    for key,val in self.adj_list_count.items():
      if val > max:
        max= val 
    for key,val in self.adj_list_count.items():
      if val == max:
        print("Node with the most number of connections:",key)

"""Test case 1

Input :1 -> 2 -> 3 -> 5 -> 2 -> 1 <br>
Output:2 <br>
*since router 2 has 2 inbound links and 2 outbound links
"""

edges = [(1,2),(2,3),(3,5),(5,2),(2,1)]
graph = Graph(edges)
graph.identify_router()

"""Test Case 2

Input:1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6<br>
Output : 5 <br>
since router 5 has 2 inbound links and 2 outbound link
"""

edges = [(1,3),(3,5),(5,6),(6,4),(4,5),(5,2),(2,6)]
graph = Graph(edges)
graph.identify_router()

"""Test Case 3

Input  : 2 -> 4 -> 6 -> 2 -> 5 -> 6 <br>
Output : 2, 6 <br>
since router 2 has 1 inbound link and 2 outbound links and 6 has 2 inbound links and 1 outbound link
"""

edges = [(2,4),(4,6),(6,2),(2,5),(5,6)]
graph = Graph(edges)
graph.identify_router()

"""<br>

Time Complexity:

Time Complexity : O(n2) <br>

In counting of the inbound links every node is checked with every other Node in the graph.
"""