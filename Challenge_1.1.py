# -*- coding: utf-8 -*-
"""String Compression.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1xDWEg37IR9fyee0Vgo9WZ2WkXQDgFSMt

**Problem Overview**: String Compression
Implement a string compression using python. For example, aaaabbbccddddddee would become a4b3c2d6e2. If the length of the string is not reduced, return the original string.<br><br>
**Requirements<br>**
Implement a compress function which accepts an input string and returns a compressed string. Code must be implemented using python 3.6 and must follow strictly pep8 rules.<br>
Provide comments regarding the implementation.<br><br>
**Test Cases<br>**
assert compress(‘bbcceeee’) == ‘b2c2e4’<br>
assert compress(‘aaabbbcccaaa’) == ‘a3b3c3a3’<br>
assert compress(‘a’) = a<br>
Explanation<br>
Explain time complexity of the compression written.
"""

def assert_compress(str):
  list_store=[]
  for i in range(0,len(str)):    
    #Loop over i : every element in the string
    if i == 0:
      #Check for the position of the ith element of the string
      #If i=0,insert first element into list
      list_store.append(str[i])

    else:
      #If not first element 
      #Check the ith element equals the element in list
      if str[i] == list_store[-1]:
        #If True,insert into list
        list_store.append(str[i])
       
      else:
        #ith element is not equal to the last element in list

        #print first element in list & length of list 
        print(list_store[0],end="")
        print(len(list_store),end="")

        #Flush the list to store the new element
        list_store=[]
        list_store.append(str[i])

  print(list_store[0],end="")
  if len(list_store) > 2:
    print(len(list_store),end="")

assert_compress('bbcceeee')

assert_compress('aaabbbcccaaa')

assert_compress('a')

"""Time Complexity : O(N)

The running time of the assert_compress function is proportional to the length of the input string.
"""